using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    private bool puedoSaltar = false;
    private SpriteRenderer sr;
    private Animator animator;
    private Rigidbody2D rb2d;
    public float correrAuto = 10;
    public int muerto = 0;
    public int numSaltos = 0;
    void Start() //Se ejecuta una única vez
    {
        sr = GetComponent<SpriteRenderer>(); //obtengo el objeto SpriteRenderer de Player
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() //SE ejecuta siempre o que siempre esta en ejecución
    {        
       
        rb2d.velocity = new Vector2(correrAuto, rb2d.velocity.y);
        if (numSaltos == 10)
        {
            correrAuto += 2;
            numSaltos = 0;
        }
            SetRunAnimation();
        if(muerto == 3)
        {
            SetDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);            
        }

        else if(Input.GetKeyDown(KeyCode.UpArrow) && puedoSaltar)
        {
            var jump = 50f;
            rb2d.velocity = Vector2.up * jump;
            puedoSaltar = false;   
            SetJumpAnimation();        
        }
        
    }    

    void OnCollisionEnter2D(Collision2D other)
    {
        puedoSaltar = true;
        if(other.gameObject.layer == 6)
        {
            muerto = 3;
            SetExitAnimation();          
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        puedoSaltar = true;
        if (other.gameObject.layer == 6)
        {
            numSaltos += 1;
        }
    }
    private void SetIdleAnimation()
    {
        animator.SetInteger("Estado",0);

    }
    private void SetRunAnimation()
    {
        animator.SetInteger("Estado",1);

    }      
    
    private void SetJumpAnimation()
    {
        animator.SetInteger("Estado",2);

    }
    private void SetDeadAnimation()
    {
        animator.SetInteger("Estado",3);

    }

    private void SetExitAnimation()
    {
        animator.SetInteger("Estado",4);
    }
}