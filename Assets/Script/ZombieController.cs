using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    // Start is called before the first frame update
    private float correrAuto = 2;
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        {      
            rb2d.velocity = Vector2.up * correrAuto;            
        }
        else    
        {
        rb2d.velocity = new Vector2(-correrAuto, rb2d.velocity.y); 
        }       
    }
}
